<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 2/4/2017
 * Time: 11:21 AM
 */

namespace App\City;
use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;

class City extends DB
{
    private $id;
    private $country;
    private $city;



    public function setData($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('country',$postData)){
            $this->country = $postData['country'];
        }

        if(array_key_exists('city',$postData)){
            $this->city = $postData['city'];
        }

    }


    public function store(){

        $arrData = array($this->country,$this->city);

        $sql = "INSERT into city(country,city) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }



    public function index(){

        $sql = "select * from city where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from city where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from city where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}