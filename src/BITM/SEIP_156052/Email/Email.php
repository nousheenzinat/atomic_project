<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 2/4/2017
 * Time: 9:35 AM
 */

namespace App\Email;

use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;
class Email extends DB
{


    private $id;
    private $name;
    private $email;



    public function setData($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('name',$postData)){
            $this->name = $postData['name'];
        }

        if(array_key_exists('email',$postData)){
            $this->email = $postData['email'];
        }

    }


    public function store(){

        $arrData = array($this->name,$this->email);

        $sql = "INSERT into email(name,email) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }



    public function index(){

        $sql = "select * from email where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from email where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from email where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }



}