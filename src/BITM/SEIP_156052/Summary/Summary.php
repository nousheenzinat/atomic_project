<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 2/4/2017
 * Time: 10:46 AM
 */

namespace App\Summary;
use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;

class Summary extends DB
{
    private $id;
    private $org;
    private $summary;



    public function setData($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('org',$postData)){
            $this->org = $postData['org'];
        }

        if(array_key_exists('summary',$postData)){
            $this->summary = $postData['summary'];
        }

    }


    public function store(){

        $arrData = array($this->org,$this->summary);

        $sql = "INSERT into summary(org,summary) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }



    public function index(){

        $sql = "select * from summary where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from summary where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from summary where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}