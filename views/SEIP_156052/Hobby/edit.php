<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";




$objHobby = new \App\Hobby\Hobby();
$objHobby->setData($_GET);
$oneData = $objHobby->view();
$str=explode(",",$oneData->hobby);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Create Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

    <div class="navbar">

        <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td>

    </div>



    <form  class="form-group f" action="update.php" method="post">

        Enter your Name:
        <input class="form-control" type="text" name="name" value="<?php echo $oneData->name ?>" >
        <br>
        Enter your hobby:
        <input type="checkbox" name="hobby[]" value="swimming" <?php if(in_array("swimming",$str)){echo "checked";} ?>><label>swimming</label>
        <input type="checkbox" name="hobby[]" value="writing" <?php if(in_array("writing",$str)){echo "checked";} ?> ><label>writing</label>
        <input type="checkbox" name="hobby[]" value="reading" <?php if(in_array("reading",$str)){echo "checked";} ?> ><label>reading</label><br>
        <input type="checkbox" name="hobby[]" value="gardening" <?php if(in_array("gardening",$str)){echo "checked";} ?> ><label>gardening</label><br>

        <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >

        <input class="btn btn-primary" type="submit" value="Update">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


