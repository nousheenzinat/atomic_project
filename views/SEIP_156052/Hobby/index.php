<?php
require_once("../../../vendor/autoload.php");

$objHobby = new \App\Hobby\Hobby();

$allData = $objHobby->index();

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
     $msg = Message::getMessage();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);




//pagination start
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;


$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objHobby->indexPaginator($page,$itemsPerPage);



$serial = (  ($page-1) * $itemsPerPage ) +1;



if($serial<1) $serial=1;

####/pagination
echo "<div style='height: 30px'> <div  id='message'> $msg </div> </div>";

?>



<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Book Title - Active List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
    </style>



</head>
<body>


<div class="container">

    <form id="searchForm" action="index.php"  method="get" style="margin-top: 45px">
        <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
        <input type="checkbox"  name="byname"   checked  >By Name
        <input type="checkbox"  name="byhobby"  checked >By Hobby
        <input hidden type="submit" class="btn-primary" value="search">
    </form>




    //
    <form action="trashmultiple.php" method="post" id="multiple">


        <div class="navbar"?>
            <a href="trashed.php?Page=1"   class="btn btn-info role="button"> View Trashed List</a> &nbsp;&nbsp;&nbsp;
            <a href="create.php "  class="btn btn-primary role="button"> Add new Book</a>&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" id="delete">Delete  Selected</button>&nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn btn-warning">Trash Selected</button>

        </div>

        <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>

    <div class="navbar">
        <td><a href='trashed.php' class='btn btn-group-lg btn-info'>Trashed-List</a> </td>

        <td><a href='create.php' class='btn btn-group-lg btn-info'>Add</a> </td>

    </div>

    <h1 style="text-align: center" ;">Hobby List</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">

        <tr>
            <th>Select all  <input id="select_all" type="checkbox" value="select all"></th>


        <tr>
            <th style='width: 10%; text-align: center'>Serial Number</th>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>name</th>
            <th>hobby</th>
            <th>Action Buttons</th>
        </tr>

        <?php
        $serial= 1;
        foreach($allData as $oneData){

            if($serial%2) $bgColor = "#cccccc";
            else $bgColor = "#ffffff";

            echo "

                  <tr  style='background-color: $bgColor'>

                  <td style='padding-left: 6%'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                     <td style='width: 10%; text-align: center'>$serial</td>
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->hobby</td>

                     <td><a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                     <a href='edit.php?id=$oneData->id' class='btn btn-primary'>Edit</a>
                     <a href='trash.php?id=$oneData->id' class='btn btn-warning'>Soft Delete</a>
                     <a href='delete.php?id=$oneData->id' class='btn btn-danger'>Delete</a>

                     </td>
                  </tr>
              ";
            $serial++;
        }
        ?>

    </table>

</form>
    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
    <div align="left" class="container">
        <ul class="pagination">

            <?php

            $pageMinusOne  = $page-1;
            $pagePlusOne  = $page+1;


            if($page>$pages) Utility::redirect("index.php?Page=$pages");

            if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";


            for($i=1;$i<=$pages;$i++)
            {
                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

            }
            if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

            ?>

            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                <?php
                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                ?>
            </select>
        </ul>
    </div>
    <!--  ######################## pagination code block#2 of 2 end ###################################### -->



















</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->







</body>
</html>