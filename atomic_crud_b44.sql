-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2017 at 08:08 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_crud_b44`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(12) NOT NULL,
  `name` varchar(122) NOT NULL,
  `birthday` datetime NOT NULL,
  `soft_deleted` varchar(123) DEFAULT 'NO'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`, `soft_deleted`) VALUES
(1, 'nousheen', '0000-00-00 00:00:00', 'NO'),
(2, 'suraia', '0000-00-00 00:00:00', 'NO'),
(3, 'ZINAT FARHANA CHOWDHURY', '0000-00-00 00:00:00', 'NO'),
(4, 'ZINAT FARHANA CHOWDHURY', '0000-00-00 00:00:00', 'NO'),
(5, 'nousheen', '0000-00-00 00:00:00', 'NO'),
(6, 'nousheen', '0000-00-00 00:00:00', 'NO'),
(7, 'sm', '2017-02-07 00:00:00', 'NO'),
(8, 'sm', '2017-02-07 00:00:00', 'NO'),
(9, 'sm', '2017-02-07 00:00:00', 'NO'),
(10, 'sm', '2017-02-07 00:00:00', 'NO'),
(11, 'sm', '2017-02-07 00:00:00', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'Pyramid', 'Tom Martin', 'No'),
(2, 'Inferno', 'Dan Brown', 'No'),
(3, 'Himu', 'Humayun Ahmed', 'Yes'),
(4, 'vcchg', 'fdsfd', 'No'),
(5, 'gdfgd', '547646', 'Yes'),
(6, 'ertdy', '346477', 'No'),
(7, 'fdsd fs', 'sfd sf df', 'No'),
(8, 'Durbin', 'Shirshendu', 'Yes'),
(9, 'sfds', 'sdfertwt3', 'No'),
(10, 'dfs', 'fds sar', 'No'),
(11, 'sdfsf', 'sdfsg', 'No'),
(12, 'Mr Y', 'Mr X', 'No'),
(13, 'fdgdfg', 'dsfgdsdyr', 'No'),
(14, 'stest', 'fdsgdsg', 'No'),
(15, 'sdfsfs', 'sfdsf', 'No'),
(16, 'sdgfs', 'sdgf', 'No'),
(17, 'dfs fd', 'sdf sgt ert', 'No'),
(18, 'dfs fd', 'sdf sgt ert', 'No'),
(19, 'dsfs f', 'dy rt ryry', 'No'),
(20, 'dsfs f', 'dy rt ryry', 'No'),
(21, 'dfsf', 'sdfsf', 'No'),
(22, 'Amar Golpo', 'Mahbubur Rahman', 'YES');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(12) NOT NULL,
  `country` varchar(123) COLLATE ucs2_unicode_ci NOT NULL,
  `city` varchar(121) COLLATE ucs2_unicode_ci NOT NULL,
  `soft_deleted` varchar(12) COLLATE ucs2_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(12) NOT NULL,
  `name` varchar(123) COLLATE ucs2_unicode_ci NOT NULL,
  `email` varchar(123) COLLATE ucs2_unicode_ci NOT NULL,
  `soft_deleted` varchar(123) COLLATE ucs2_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=ucs2 COLLATE=ucs2_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_deleted`) VALUES
(1, 'nousheen', 'arifool_hasan@yahoo.com', 'NO'),
(2, 'suraia', 'smriti_cse@yahoo.com', 'NO'),
(3, 'saima', 's@gmail.com', 'NO'),
(4, 'rafi', 'rafi@ymail.com', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(12) NOT NULL,
  `name` varchar(123) COLLATE ucs2_unicode_ci NOT NULL,
  `gender` varchar(134) COLLATE ucs2_unicode_ci NOT NULL,
  `soft_deleted` varchar(123) COLLATE ucs2_unicode_ci DEFAULT 'NO'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=ucs2 COLLATE=ucs2_unicode_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `soft_deleted`) VALUES
(1, 'nousheen', 'Female', 'NO'),
(2, 'suraia', 'Female', 'NO'),
(3, 'saima', 'Female', 'NO'),
(4, 'nihal', 'male', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
`id` int(12) NOT NULL,
  `name` varchar(122) NOT NULL,
  `hobby` varchar(112) NOT NULL,
  `soft_deleted` varchar(223) DEFAULT 'NO'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobby`, `soft_deleted`) VALUES
(2, 'suraia', 'swimming,writing,reading', 'No'),
(3, 'ZINAT FARHANA CHOWDHURY', 'swimming,gardening', 'No'),
(5, 'ttt', 'swimming', 'Yes'),
(6, 'nousheen', 'swimming,gardening', 'Yes'),
(7, 'sada', 'gardening', 'NO'),
(8, 'nousheen', 'reading', 'NO'),
(9, 'munira', 'gardening', 'Yes'),
(10, 'tasnin', 'Array', 'No'),
(11, 'sadia', 'swimming,writing', 'NO'),
(13, 'suria', 'swimming,writing,gardening', 'NO'),
(15, 'muniraa jahan sadia', 'swimming,reading', 'NO'),
(16, 'Tasnin', 'reading,gardening', 'No'),
(17, 'Riva', 'reading', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE IF NOT EXISTS `summary` (
`id` int(12) NOT NULL,
  `org` varchar(123) COLLATE ucs2_unicode_ci NOT NULL,
  `summary` varchar(123) COLLATE ucs2_unicode_ci NOT NULL,
  `soft_deleted` varchar(12) COLLATE ucs2_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `summary`
--
ALTER TABLE `summary`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
